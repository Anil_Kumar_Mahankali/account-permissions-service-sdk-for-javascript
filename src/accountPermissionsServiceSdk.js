import AccountPermissionsServiceSdkConfig from './accountPermissionsServiceSdkConfig';
import DiContainer from './diContainer';
import AccountPermissionsSynopsisView from './accountPermissionsSynopsisView';
import GetAccountPermissionsWithAccountIdFeature from './getAccountPermissionsWithAccountIdFeature';

/**
 * @class {AccountPermissionsServiceSdk}
 */
export default class AccountPermissionsServiceSdk {

    _diContainer:DiContainer;

    /**
     * @param {AccountPermissionsServiceSdkConfig} config
     */
    constructor(config:AccountPermissionsServiceSdkConfig) {

        this._diContainer = new DiContainer(config);

    }

    /**
     * @param {string} accountId
     * @param {string} accessToken
     * @returns {AccountPermissionsSynopsisView}
     */
    getAccountPermissionsWithId(
        accountId:string,
        accessToken:string):AccountPermissionsSynopsisView {

        return this
            ._diContainer
            .get(GetAccountPermissionsWithAccountIdFeature)
            .execute(accountId,accessToken);

    }

}

