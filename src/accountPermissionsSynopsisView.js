/**
 * @class {AccountPermissionsSynopsisView}
 */
export default class AccountPermissionsSynopsisView {

    _id:number;

    _accountId:string;

    _spiff:boolean;

    _extendedWarranty:boolean;

    _startDate:string;

    _endDate:string;

    /**
     * @param {number} id
     * @param {string} accountId
     * @param {boolean} spiff
     * @param {boolean} extendedWarranty
     * @param {string} startDate
     * @param {string} endDate
     */
    constructor(id:number,
                accountId:string,
                spiff:boolean,
                extendedWarranty:boolean,
                startDate:string,
                endDate:string) {

        if (!id) {
            throw new TypeError('id required');
        }
        this._id = id;

        if (!accountId) {
            throw new TypeError('accountId required');
        }
        this._accountId = accountId;

        if (!spiff) {
            throw new TypeError('spiff required');
        }
        this._spiff = spiff;

        if (!extendedWarranty) {
            throw new TypeError('extendedWarranty required');
        }
        this._extendedWarranty = extendedWarranty;

        if (!startDate) {
            throw new TypeError('startDate required');
        }
        this._startDate = startDate;

        if (!endDate) {
            throw new TypeError('endDate required');
        }
        this._endDate = endDate;

    }

    /**
     * @returns {number}
     */
    get id():number {
        return this._id;
    }

    /**
     * @returns {string}
     */
    get accountId():string {
        return this._accountId;
    }

    /**
     * @returns {boolean}
     */
    get spiff():boolean {
        return this._spiff;
    }

    /**
     * @returns {boolean}
     */
    get extendedWarranty():boolean {
        return this._extendedWarranty;
    }

    /**
     * @returns {string}
     */
    get startDate():string {
        return this._startDate;
    }

    /**
     * @returns {string}
     */
    get endDate():string {
        return this._endDate;
    }

}