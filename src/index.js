/**
 * @module
 * @description discount code service sdk public API
 */
export {default as AccountPermissionsServiceSdkConfig } from './accountPermissionsServiceSdkConfig'
export {default as AccountPermissionsSynopsisView} from './accountPermissionsSynopsisView';
export {default as default} from './accountPermissionsServiceSdk';
