Feature: Get Account Permissions
  Get  account permissions synopsis view object with account id

  Scenario: Success
    Given I provide a valid accessToken
    When I execute getAccountPermissionsWithId
    Then account permissions synopsis view object in the account-pemissions-service is returned